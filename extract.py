import os
import time

directories = ["01", "02", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26"]
file_list = ["749", "374", "381", "100", "214", "331"]
gene_list = open("mouse_expression_data_sets.csv", "r").readlines()


start = time.time()
gene_symbol = ""
output = open("output.csv", "w+")

for folder in directories:
    list = os.listdir(folder)

    for file in list:
        file = folder + "/" + file
        open_file = open(file, "r").readlines()
        print(file)

        for line in open_file:
            lines = line.split(",")

            exp_id = lines[0]
            gene_id = lines[1]
            structure = lines[2]
            a = lines[7]
            b = lines[8]


            for each_line in gene_list:
                all_lines = each_line.split(",")

                id = all_lines[0]

                if id == gene_id:
                    gene_symbol = all_lines[2]
                    print(gene_symbol)
                    print("\n")

                    output.write("{},{},{},{},{},{}\n".format(exp_id, structure, gene_id, gene_symbol, a, b))


            if gene_symbol == "":
                gene_symbol = "Unknown"
                output.write("{},{},{},{},{}\n".format(exp_id, structure, gene_id, gene_symbol, a, b))



end = time.time() - start

print(end / 60)
