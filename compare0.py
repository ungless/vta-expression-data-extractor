import os

local = open("output.csv", "r").readlines()
test = open("test.csv", "r").readlines()
output = open("final_output.csv", "w")
printed = False
file_list = ["749", "374", "381", "100", "214", "331"]

for line in test:
    line = line.rstrip("\n")
    commas = line.split(",")
    gene_symbol = commas[0]
    print("\n")
    print(gene_symbol)

    for file in file_list:
        current_file = open("{}.csv".format(file)).readlines()

        for gene in current_file:
            gene = gene.rstrip("\n")
            split = gene.split(",")
            local_gene_symbol = split[3]

            if gene_symbol == local_gene_symbol:
                printed = True

                if file == "749":
                    s1 = gene
                elif file == "374":
                    s2 = gene
                elif file == "381":
                    s3 = gene
                elif file == "100":
                    s4 = gene
                elif file == "214":
                    s5 = gene
                elif file == "331":
                    s6 = gene

                print("Success")
            else:
                continue
    try:
        output.write("{},{},{},{},{},{},{}\n".format(line,s1,s2,s3,s4,s5,s6))
    except NameError:
        s1 = "N/A"
        s2 = "N/A"
        s3 = "N/A"
        s4 = "N/A"
        s5 = "N/A"
        s6 = "N/A"
        output.write("{},{},{},{},{},{},{}\n".format(line,s1,s2,s3,s4,s5,s6))

# for file in file_list:
#     current_file = open("{}.csv".format(file)).readlines()
#     print(gene_symbol)
#     for current_line in current_file:
#         separated = current_line.split(",")
#         current_file_gene = separated[3]
#         print(current_file_gene)
#         if current_file_gene == gene_symbol:
#             printed = True
#
#             if file == "749":
#                 s1 = "{}, {}".format(file, line)
#             elif file == "374":
#                 s2 = "{}, {}".format(file, line)
#             elif file == "381":
#                 s3 = "{}, {}".format(file, line)
#             elif file == "100":
#                 s4 = "{}, {}".format(file, line)
#             elif file == "214":
#                 s5 = "{}, {}".format(file, line)
#             elif file == "331":
#                 s6 = "{}, {}".format(file, line)
